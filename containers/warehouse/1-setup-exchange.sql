DROP TABLE IF EXISTS crypto.exchanges;
DROP TABLE IF EXISTS crypto.markets;
DROP TABLE IF EXISTS crypto.prices;
DROP SCHEMA IF EXISTS crypto;
CREATE SCHEMA crypto;

CREATE TABLE crypto.exchanges (
    id VARCHAR(50),
    name VARCHAR(50),
    rank INT,
    percentTotalVolume NUMERIC(8, 5),
    volumeUsd NUMERIC,
    tradingPairs INT,
    socket BOOLEAN,
    exchangeUrl VARCHAR(50),
    updated_unix_millis BIGINT,
    updated_utc TIMESTAMP
);

CREATE TABLE crypto.markets (
    id VARCHAR(50),
    rank INT,
    baseSymbol VARCHAR(50),
    baseId VARCHAR(50),
    quoteSymbol VARCHAR(50),
    quoteId VARCHAR(50),
    priceQuote NUMERIC,
    priceUsd NUMERIC,
    volumeUsd24Hr NUMERIC,
    percentExchangeVolume NUMERIC(8, 5),
    tradesCount24Hr INT,
    updated BIGINT,
    updated_utc TIMESTAMP
);

CREATE TABLE crypto.prices (
    id VARCHAR(50),
    quoteId VARCHAR(50),
    time_period_start TIMESTAMP,
    time_period_end TIMESTAMP,
    time_open TIMESTAMP,
    time_close TIMESTAMP,
    rate_open NUMERIC,
    rate_high NUMERIC,
    rate_low NUMERIC,
    rate_close NUMERIC
);