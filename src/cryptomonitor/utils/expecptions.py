'''Contains all custom exceptions.'''
from requests.api import request

class ApiException(Exception):
    '''Custom Exception class to handle Api related expected errors.'''
    def __init__(self, message: str, request: request):
        self.request = request
        self.message = message
