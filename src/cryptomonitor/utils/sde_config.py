'''Contains functions to manage data warehouse credentials.'''

import os

from cryptomonitor.utils.db import DBConnection


def get_warehouse_creds() -> DBConnection:
    '''Fetches and returns the warehouse credentials stored in the env file.'''
    return DBConnection(
        user=os.getenv('POSTGRES_USER', ''),
        password=os.getenv('POSTGRES_PASSWORD', ''),
        db=os.getenv('POSTGRES_DB', ''),
        host=os.getenv('POSTGRES_HOST', ''),
        port=int(os.getenv('POSTGRES_PORT', 5432)),
    )
