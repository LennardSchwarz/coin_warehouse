'''Contains data warehouse related functions'''
from contextlib import contextmanager
from dataclasses import dataclass

import psycopg2


@dataclass
class DBConnection:
    '''Contains information needed for the WarehouseConnection class.'''
    db: str
    user: str
    password: str
    host: str
    port: int = 5432


class WarehouseConnection:
    '''Establishes a connection to the PostgreSQL data warehouse.'''
    def __init__(self, db_conn: DBConnection):
        self.conn_url = (
            f'postgresql://{db_conn.user}:{db_conn.password}@'
            f'{db_conn.host}:{db_conn.port}/{db_conn.db}'
        )

    @contextmanager
    def managed_cursor(self, cursor_factory=None):
        '''Manages the state of the warehouse connections.'''
        self.conn = psycopg2.connect(self.conn_url)
        self.conn.autocommit = True
        self.curr = self.conn.cursor(cursor_factory=cursor_factory)
        try:
            yield self.curr
        finally:
            self.curr.close()
            self.conn.close()
