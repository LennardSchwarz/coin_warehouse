'''Module to run the ETL processes'''
import datetime
import logging
import os
import sys
from abc import abstractmethod, ABC
from dataclasses import dataclass
from typing import Any, List, Optional
from dateutil.relativedelta import relativedelta
import psycopg2.extras as p
import requests


import cryptomonitor.utils.expecptions as exceptions
import cryptomonitor.warehouse_injects.warehouse_sql_inject_queries as sqls
from cryptomonitor.utils.db import WarehouseConnection
from cryptomonitor.utils.sde_config import get_warehouse_creds

# set global logging config
logging.basicConfig(
    filename='pipelinerunner.log',
    format='%(asctime)s: %(name)s: %(levelname)s: %(message)s',
    datefmt='%d.%m.%y %H:%M:%S',
    level='INFO'
)

@dataclass(init=True, frozen=True, order=True)
class Coin:
    '''Represents the crypto currencies which can passes to the CoinApi api'''
    id: str = 'BTC'
    symbol: str = 'bitcoin'
    quote_id: str = 'USD'


class Pipeline(ABC):
    '''Represents the blueprint for the ETL process:

    Extract from API > Transform > Load into data warehouse
    '''
    def __init__(self, data_table: str, data: List = []) -> None:
        self.data_table = data_table
        self.data = data

    @abstractmethod
    def get_api_data(self):
        '''Extracts data from an api.

        Returns the extracted data.'''

    def send_to_warehouse(self) -> None:
        '''Sends data to the PostgreSQL data warehouse.'''
        data = self.data
        data_table = self.data_table
        with WarehouseConnection(
            get_warehouse_creds()
        ).managed_cursor() as curr:
            p.execute_batch(curr, data_table, data)

class CoinCap_Pipeline(Pipeline):
    '''Runs the ETL process to extract data from the CoinCap.io Api, 
    transform it and load it into our data warehouse.'''
    def __init__(self, url: str, token: str, data_table: str, data: List = None) -> None:
        super().__init__(data_table, data)  # inherit from parent class
        self.url = url
        self.token = token

    def get_api_data(self):
        '''Extracts data from the CoinCap.io Api.

        Returns the extracted data.'''
        headers = {'Authorization': 'Bearer {}'.format(self.token)}
        logging.info('Start coincap.io api call')

        try:
            response = requests.get(self.url, headers)
            # raise exception for "status other than 2xx" response
            if not 200 <= response.status_code < 300:
                message = "There was an error with the request with url: {} \n Response text: {} \n Header: {}".format(
                    self.url, response.text, response.headers
                    )
                raise exceptions.ApiException(message, response)

        except requests.ConnectionError as connection_error:
            logging.error(f"There was an error with the request, {connection_error}")
            sys.exit(1)

        self.data = response.json().get('data', [])
        return self

    def append_utc_timestamp(self):
        '''Helper function to append a unix timestamp fetched from the column 'updated' to the column 'update_dt'.'''

        def get_utc_from_unix_time(unix_ts: Optional[Any], second: int = 1000):
            '''Helper function to transform a unix timestamp to a utc datetime object.'''
            date = datetime.datetime.utcfromtimestamp(int(unix_ts) / second)
            # Adds missing trailing 'Z' from python datetime module
            date.strftime('%Y-%m-%dT%H:%M:%SZ')
            return date if unix_ts else None

        for date in self.data:
            date['update_dt'] = get_utc_from_unix_time(date.get('updated'))
        return self


class CoinApi_Pipeline(Pipeline):
    '''Runs the ETL process to extract data from CoinApi.io, 
    transform it and load it into our data warehouse.'''
    def __init__(
        self,
        data_table: str,
        token: str,
        coin: Coin = Coin('BTC', 'bitcoin'),
        start_date: str = '2020-01-01T00:00:00',
        data: List = None,
    ) -> None:
        super().__init__(data_table, data)  # inherit from parent class
        self.start_date = start_date
        self.coin = coin
        self.token = token


    def get_api_data(self):
        '''Extracts data from the CoinCap.io Api.

        Returns the extracted data.'''
        headers = {'X-CoinAPI-Key': '{}'.format(self.token)}
        start_date = self.start_date
        now = datetime.datetime.now().replace(microsecond=0).isoformat() # ISO 8601
        url = 'https://rest.coinapi.io/v1/exchangerate/{}/{}/history?period_id=1DAY&time_start={}&time_end={}&limit=1000'.format(
            self.coin.id,self.coin.quote_id, start_date, now
        )
        logging.info('Start coinapi.io call for coin {}'.format(self.coin))

        try:
            response = requests.get(url, headers=headers)
            # Catch "status other than 2xx" response
            if not response.status_code // 100 == 2:
                message = "There was an error with the request with url: {} \n Response text: {} \n Header: {}".format(
                    url, response.text, response.headers
                    )
                raise exceptions.ApiException(message, response)

        # Catch any other expection
        except requests.ConnectionError as connection_error:
            logging.error(f"There was an error with the request, {connection_error}")
            sys.exit(1)

        # parse request into .json
        self.data = response.json()

        return self

    def add_columns(self):
        '''Adds an 'id', 'symbol' and a 'quouteId' column to each dictionary element
         of the data attribute of the class.'''
        coin = self.coin
        for dic in self.data:
            dic.update(
                [
                    ('id', coin.id),
                    ('symbol', coin.symbol),
                    ('quoteId', coin.quote_id),
                ]
            )
        return self

def run_exchanges_data_etl(url: str, token: str, data_table: str) -> None:
    '''Initiates a CoinCap_Pipeline class instance and runs the exchanges etl process'''
    exchanges_etl = CoinCap_Pipeline(url, token, data_table)

    exchanges_etl.get_api_data().append_utc_timestamp().send_to_warehouse()

def run_markets_data_etl(url: str, token: str, data_table: str) -> None:
    '''Initiates a CoinCap_Pipeline class instance and runs the markets etl process'''
    markets_etl = CoinCap_Pipeline(url, token, data_table)

    markets_etl.get_api_data().append_utc_timestamp().send_to_warehouse()

def run_prices_data_etl(coinList: List, start_date: str, token: str, data_table: str) -> None:
    '''Initiates a CoinApi_Pipeline class instance and runs the prices etl process'''
    for coin in coinList:
        prices_etl = CoinApi_Pipeline(coin=coin, token=token, data_table=data_table, start_date=start_date)

        prices_etl.get_api_data().add_columns().send_to_warehouse()

if __name__ == '__main__':
    # Params CoinCap instances
    URL_COINCAP_EXCHANGES = 'https://api.coincap.io/v2/exchanges'
    URL_COINCAP_MARKETS = 'https://api.coincap.io/v2/markets'
    COINCAP_TOKEN = os.getenv('COINCAP_API_TOKEN', '')

    # Params CoinApi instance
    COINAPI_TOKEN = os.getenv('COINAPI_API_TOKEN', '')
    coinList = [
        Coin('BTC', 'bitcoin'),
        Coin('ETH', 'etherum'),
        Coin('BNB', 'binance'),
        Coin('DOGE', 'doge')
    ]

    # make start_date_prices dynamic and always two years dating back to limit free api calls qoute from CoinApi.io
    start_date_prices =(datetime.datetime.now() - relativedelta(years=2)).replace(microsecond=0).isoformat()

    try:
        run_exchanges_data_etl(
            url=URL_COINCAP_EXCHANGES,
            token=COINCAP_TOKEN,
            data_table=sqls.COINCAP_EXCHANGES_SQL
            )
    except exceptions.ApiException as exception:
        logging.warning(exception.message)

    try:
        run_markets_data_etl(
        url=URL_COINCAP_MARKETS,
        token=COINCAP_TOKEN,
        data_table=sqls.COINCAP_MARKETS_SQL
        )
    except exceptions.ApiException as exception:
        logging.warning(exception.message)

    try:
        run_prices_data_etl(
            coinList=coinList,
            start_date=start_date_prices,
            token=COINAPI_TOKEN,
            data_table=sqls.COINAPI_PRICES_SQL
            )
    except exceptions.ApiException as exception:
        logging.warning(exception.message)
