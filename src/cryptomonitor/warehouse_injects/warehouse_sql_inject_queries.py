'''Contains SQL statements to inject into the etl process. 

Maps the extracted api data to the data warehouse schema.'''
COINCAP_EXCHANGES_SQL = '''
    INSERT INTO crypto.exchanges (
        id,
        name,
        rank,
        percenttotalvolume,
        volumeusd,
        tradingpairs,
        socket,
        exchangeurl,
        updated_unix_millis,
        updated_utc
    )
    VALUES (
        %(exchangeId)s,
        %(name)s,
        %(rank)s,
        %(percentTotalVolume)s,
        %(volumeUsd)s,
        %(tradingPairs)s,
        %(socket)s,
        %(exchangeUrl)s,
        %(updated)s,
        %(update_dt)s
    );
    '''

COINCAP_MARKETS_SQL = '''
    INSERT INTO crypto.markets (
    id,
    rank,
    baseSymbol,
    baseId,
    quoteSymbol,
    quoteId,
    priceQuote,
    priceUsd,
    volumeUsd24Hr,
    percentExchangeVolume,
    tradesCount24Hr,
    updated,
    updated_utc
    )
    VALUES (
        %(exchangeId)s,
        %(rank)s,
        %(baseSymbol)s,
        %(baseId)s,
        %(quoteSymbol)s,
        %(quoteId)s,
        %(priceQuote)s,
        %(priceUsd)s,
        %(volumeUsd24Hr)s,
        %(percentExchangeVolume)s,
        %(tradesCount24Hr)s,
        %(updated)s,
        %(update_dt)s
    );
    '''

COINAPI_PRICES_SQL = '''
    INSERT INTO crypto.prices (
    id,
    quoteId,
    time_period_start,
    time_period_end,
    time_open,
    time_close,
    rate_open,
    rate_high,
    rate_low,
    rate_close
    )
    VALUES (
        %(id)s,
        %(quoteId)s,
        %(time_period_start)s,
        %(time_period_end)s,
        %(time_open)s,
        %(time_close)s,
        %(rate_open)s,
        %(rate_high)s,
        %(rate_low)s,
        %(rate_close)s
    );
    '''
