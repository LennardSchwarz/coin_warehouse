# Crypto Warehouse
This ETL pipeline extracts various types of crypto currency data from [CoinCap.io](https://docs.coincap.io/) and [CoinApi.io](https://docs.coinapi.io/), transforms it and loads it into a data warehouse.

## Architecture

![Architecture](assets/images/architecture.png)

I use [python](src\cryptomonitor\data_etl.py) to extract, transform and load data. The warehouse is a PostgreSQL database. The ETL process is scheduled with [Ofelia](https://github.com/mcuadros/ofelia). A [Metabase dashboard](https://www.metabase.com/learn/getting-started/getting-started.html) is used as a presentation layer. 

Finally, a [PostgREST API](https://hub.docker.com/r/postgrest/postgrest) with a [swagger UI](https://hub.docker.com/r/swaggerapi/swagger-ui) allow read-only access to the warehouse data.

All of the components are running as docker containers in an AWS EC2 ubuntu instance. Tests are conducted with pytest.

# How to access services
The Metabase dashboard is exposed on port 3000:
```bash
#dev 
localhost:3000

#prod
public-ec2-link.com:3000
```

The REST API is exposed on port 3001:
```bash
#dev
localhost:3001

#prod
public-ec2-link.com:3001
```

The swagger UI is exposed on port 8080:
```bash
#dev
localhost:8080

#prod
public-ec2-link.com:8080
```

Use swagger with the link of the API to learn about the data model. 

# Future work / Improvements
This is what I want to work on next:
- Improve integration testing and end-to-end testing
- Fix CI/CD Gitlab pipeline
- Add data quality testing
- Improve exception handling in extract and load script
- Implement streaming data from APIs via ws instead of mini-batches
- Implement data cleanup job to remove old data
- Move data storage from PostgreSQL docker instance to AWS S3 Bucket 


# Development notes
### Pre-requisites
1. [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/) v1.27.0 or later.
2. [AWS account](https://aws.amazon.com/).
3. [AWS CLI installed](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html) and [configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html).
4. [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

## Local
To build and develop this project locally, follows these steps:
### Building
Build the python environment with the with the pip ['requirements.txt'](requirements.txt).

Use the commands in the [Makefile](Makefile) to spin up the docker containers.

```bash
cd crypto_warehouse
make up # builds and starts the docker containers
```
Note that you need to create your own .env file:
```bash
POSTGRES_USER=user
POSTGRES_PASSWORD=secret-password
POSTGRES_DB=finance
POSTGRES_HOST=warehouse
POSTGRES_PORT=5432
COINCAP_API_TOKEN=personal-api-token
COINAPI_API_TOKEN=personal-api-token


PGRST_DB_ANON_ROLE=anon
PGRST_DB_SCHEMA=crypto
PGRST_DB_URI=postgres://user:passwort@host:5432/db -> TODO: configure with your values
 ```


### Metabase Dashbaord
Go to http://localhost:3000 to checkout your Metabase instance.

Connect to the warehouse with the following credentials

```bash
Host: warehouse
Database name: finance
```
The remaining configs are available in the [.env](.env) file.

Refer to [this doc](https://www.metabase.com/docs/latest/users-guide/07-dashboards.html) for creating a Metabase dashboard.

### Orchestration
Schedule jobs in the [Ofelia](https://github.com/mcuadros/ofelia) [config.ini](containers/ofelia/config.ini).

### Tear down

You can spin down your local instance with:

```bash
make down
```

You can spin down and delete all docker containers with:
```bash
make hard_delete
```

## AWS
To deploy this project on AWS, follow the instructions on Joseph Machado's [GitHub project](https://github.com/josephmachado/bitcoinMonitor), on which this project is based.



# Credit
This project is based on this [blog post](https://www.startdataengineering.com/post/data-engineering-project-to-impress-hiring-managers/#introduction) and the corresponding [GitHub repository](https://github.com/josephmachado/bitcoinMonitor) from Joseph Machado.