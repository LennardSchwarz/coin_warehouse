up:
	docker compose --env-file .env up --build -d

down: 
	docker compose down

stop:
	docker compose stop

restart:
	down up

delete_images:
	docker rmi -f postgres:13 metabase/metabase pipelinerunner

delete_volumes:
	docker system prune --volumes -f

hard_delete: down delete_images delete_volumes

shell:
	docker exec -ti pipelinerunner bash

format:
	docker exec pipelinerunner python -m black -S --line-length 79 .

isort:
	docker exec pipelinerunner isort .

pytest:
	docker exec pipelinerunner pytest /code/test

type:
	docker exec pipelinerunner mypy --ignore-missing-imports /code

lint: 
	docker exec pipelinerunner flake8 /code

pylint:
	docker exec pipelinerunner pylint /code/src

#ci: isort format type lint pytest