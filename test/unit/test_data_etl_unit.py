import csv
import datetime
import json
from os import sep

import pytest

import cryptomonitor.data_etl as etl
import cryptomonitor.warehouse_injects.warehouse_sql_inject_queries as sqls

# from cryptomonitor.warehouse_injects.warehouse_sql_inject_queries import coincap_exchanges_sql, coinapi_prices_sql
# from cryptomonitor.data_etl import Coin, CoinCap_Pipeline, CoinApi_Pipeline


class Test_Pipeline:
    class Test_CoinCap_Exchanges_Pipeline:
        @pytest.fixture(scope='class')
        def CoinCap_Exchanges_Pipeline(self):
            '''Returns a CoinCap_Pipeline instance with exchange sample data already fetched from the API'''

            with open('test/fixtures/sample_coincap_exchanges.json') as f:
                data = json.load(f)

            CoinCap_Exchanges_Pipeline = etl.CoinCap_Pipeline(
                url='', token='', data_table=sqls.COINCAP_EXCHANGES_SQL, data=data
            )
            yield CoinCap_Exchanges_Pipeline

        @pytest.mark.skip(reason='method was moved inside "append_utc_timestamp"')
        def test_get_utc_from_unix_time(self, CoinCap_Exchanges_Pipeline):
            ut: int = 1625249025588
            expected = datetime.datetime(2021, 7, 2, 18, 3, 45, 588000)
            actual = CoinCap_Exchanges_Pipeline.get_utc_from_unix_time(
                unix_ts=ut
            )
            assert expected == actual

        def test_append_utc_timestamp(self, CoinCap_Exchanges_Pipeline):
            CoinCap_Exchanges_Pipeline.append_utc_timestamp()

            actual = CoinCap_Exchanges_Pipeline.data

            with open(
                'test/fixtures/sample_coincap_exchanges_update_dt.json'
            ) as f:
                expected = json.load(f)
            # convert isoformat string to datetime object
            for d in expected:
                d['update_dt'] = datetime.datetime.fromisoformat(
                    d['update_dt']
                )

            assert expected == actual

    class Test_CoinApi_Prices_Pipeline:
        @pytest.fixture(scope='class')
        def CoinApi_Bitcoin_Pipeline(self):
            '''Returns a CoinApi_Pipeline instance with bitcoin prices sample data already fetched from the API'''

            with open('test/fixtures/sample_coinapi_prices.json') as f:
                data = json.load(f)

            CoinApi_Bitcoin_Pipeline = etl.CoinApi_Pipeline(
                data_table=sqls.COINAPI_PRICES_SQL, token='', data=data, coin=etl.Coin()
            )
            yield CoinApi_Bitcoin_Pipeline

        def test_add_columns(self, CoinApi_Bitcoin_Pipeline):
            CoinApi_Bitcoin_Pipeline.add_columns()

            actual = CoinApi_Bitcoin_Pipeline.data

            with open('test/fixtures/sample_coinapi_bitcoin_prices.json') as f:
                expected = json.load(f)

            assert expected == actual
