import datetime
from decimal import Decimal
import json
import pytest

import psycopg2.extras as p
from unittest.mock import patch

import cryptomonitor.data_etl as etl
from cryptomonitor.utils.db import WarehouseConnection
from cryptomonitor.utils.sde_config import get_warehouse_creds

CRYPTO_TEST_EXCHANGES_SETUP = '''
    DROP TABLE IF EXISTS crypto.test_exchanges;
    CREATE TABLE crypto.test_exchanges (
        id VARCHAR(50),
        name VARCHAR(50),
        rank INT,
        percentTotalVolume NUMERIC(8, 5),
        volumeUsd NUMERIC,
        tradingPairs INT,
        socket BOOLEAN,
        exchangeUrl VARCHAR(50),
        updated_unix_millis BIGINT,
        updated_utc TIMESTAMP
    );
    '''

COINCAP_TEST_EXCHANGES_SQL = '''
    INSERT INTO crypto.test_exchanges (
        id,
        name,
        rank,
        percenttotalvolume,
        volumeusd,
        tradingpairs,
        socket,
        exchangeurl,
        updated_unix_millis,
        updated_utc
    )
    VALUES (
        %(exchangeId)s,
        %(name)s,
        %(rank)s,
        %(percentTotalVolume)s,
        %(volumeUsd)s,
        %(tradingPairs)s,
        %(socket)s,
        %(exchangeUrl)s,
        %(updated)s,
        %(update_dt)s
    );
    '''

def test_coincap_exchanges_end2end():
    # initialize warehouse connection
    with WarehouseConnection(
        get_warehouse_creds()
    ).managed_cursor() as curr:
        # setup
        curr.execute(CRYPTO_TEST_EXCHANGES_SETUP)

        # initialize instance
        with open('test/fixtures/sample_coincap_exchanges.json') as f:
            data = json.load(f)

        CoinCap_Exchanges_Pipeline = etl.CoinCap_Pipeline(
            url='', token='', data_table=COINCAP_TEST_EXCHANGES_SQL, data=data
        )

        # mock get_api_data() instance method and fetch sample data
        with open(
            'test/fixtures/sample_coincap_exchanges.json') as f:
                test_exchange_data = json.load(f)
        CoinCap_Exchanges_Pipeline.data = test_exchange_data

        # append utc timestamp
        CoinCap_Exchanges_Pipeline.append_utc_timestamp()
        
        # # mock send_to_warehouse() instance method and send sample data to warehouse
        p.execute_batch(curr, COINCAP_TEST_EXCHANGES_SQL, CoinCap_Exchanges_Pipeline.data)

        # fetch actual result data from warehouse
        actual_sql = '''SELECT * FROM crypto.test_exchanges;'''
        curr.execute(actual_sql)
        actual = curr.fetchall()
        
        expected = [('binance', 'Binance', 1, Decimal('25.89429'), Decimal('13719147357.0236831350459370'), 962, True, 'https://www.binance.com/', 1641730929121, datetime.datetime(2022, 1, 9, 12, 22, 9, 121000)), ('hitbtc', 'HitBTC', 2, Decimal('9.75925'), Decimal('5170583734.9696643012720764'), 964, True, 'https://hitbtc.com/', 1641730876573, datetime.datetime(2022, 1, 9, 12, 21, 16, 573000)), ('okex', 'Okex', 3, Decimal('9.37304'), Decimal('4965966858.1232016217816536'), 442, False, 'https://www.okex.com/', 1641730928831, datetime.datetime(2022, 1, 9, 12, 22, 8, 831000))]

        # teardown
        curr.execute("DROP TABLE IF EXISTS crypto.test_exchanges;")
        print('teardown')

    assert expected == actual
